import { Component, OnInit } from '@angular/core';
import { Options } from '../data-table/data-table.component';
import { Model } from '../data-table/data-table.component';

@Component({
  selector: 'app-mapa-plantillas',
  templateUrl: './mapa-plantillas.component.html',
  styleUrls: ['./mapa-plantillas.component.css']
})
export class MapaPlantillasComponent implements OnInit {

  constructor() { }

  mapaPlantillaModel: Array<ItemModel> = [

    { field: "Id", title: "Id", type: "number" },

    { field: "IdPlantilla", title: "IdPlantilla", type: "number" },

    { field: "IdConfiguracionNotificacion", title: "IdConfiguracionNotificacion", type: "number" },

    { field: "Origen", title: "Origen", type: "string" },

    { field: "Destino", title: "Destino", type: "string" },
    
  ]

  mapaPlantillasOptions: Options = new Options(this.mapaPlantillaModel, "../Odata/MapaPlantillas", "Id")

  ngOnInit() {
  }

}

class ItemModel implements Model {
  constructor(
    public field: string,
    public title: string,
    public type: string
  )
  { }
}