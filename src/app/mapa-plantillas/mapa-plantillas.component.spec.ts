import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapaPlantillasComponent } from './mapa-plantillas.component';

describe('MapaPlantillasComponent', () => {
  let component: MapaPlantillasComponent;
  let fixture: ComponentFixture<MapaPlantillasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapaPlantillasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapaPlantillasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
