import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { Observable } from "rxjs/Rx";

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { HomeComponent } from './home/home.component';
import { NotificacionesComponent } from './notificaciones/notificaciones.component';
import { PlantillasComponent } from './plantillas/plantillas.component';
import { ProyectosComponent } from './proyectos/proyectos.component';
import { MapaPlantillasComponent } from './mapa-plantillas/mapa-plantillas.component';
import { ConfiguracionNotificacionesComponent } from './configuracion-notificaciones/configuracion-notificaciones.component';
import { DataTableComponent } from './data-table/data-table.component';
//primeng modules
import { DataTableModule } from "primeng/components/datatable/datatable";
import { DialogModule } from "primeng/components/dialog/dialog";
import { ButtonModule } from 'primeng/components/button/button';
import { CalendarModule } from "primeng/components/calendar/calendar";
import { InputTextModule } from 'primeng/components/inputtext/inputtext';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HomeComponent,
    NotificacionesComponent,
    PlantillasComponent,
    ProyectosComponent,
    MapaPlantillasComponent,
    ConfiguracionNotificacionesComponent,
    DataTableComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'notificaciones',
        component: NotificacionesComponent
      },
      {
        path: "plantillas",
        component: PlantillasComponent
      },
      {
        path: "proyectos",
        component: ProyectosComponent
      },
      {
        path: "configuracion-notificaciones",
        component: ConfiguracionNotificacionesComponent
      },
      {
        path: "mapa-plantillas",
        component: MapaPlantillasComponent
      }
    ]),
    DataTableModule,
    DialogModule,
    ButtonModule,
    CalendarModule,
    InputTextModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
