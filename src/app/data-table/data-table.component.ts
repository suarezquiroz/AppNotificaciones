import { Component, OnInit, Input } from '@angular/core';
import { CrudService } from '../crud.service';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css'],
  providers: [CrudService]
})


export class DataTableComponent implements OnInit {

  //model to use in rows
  @Input() tableModel: object;
  @Input() options: Options;

  displayDialog: boolean;

  item: any = {};

  selectedItem: Object;

  newItem: boolean;

  loading: boolean;

  odata: any[];

  cols: any[];

  constructor(private crudService: CrudService) {
  }

  ngOnInit() {

    this.loading = true;
    setTimeout(() => {
      this.crudService.read(this.options.endpoint)
        .subscribe(
        data => {
          this.odata = data["value"];
        }
        );
      this.loading = false;
    }, 1000);

    this.cols = [];

    this.options.model.forEach(column => {
      this.cols.push({ field: column.field, header: column.title, type: column.type })

    });
  }
  showDialogToAdd() {
    this.newItem = true;
    this.item = {};
    this.displayDialog = true;
  }

  save() {
    let data = [...this.odata];
    if (this.newItem) {
      this.crudService.create(this.options.endpoint, this.item).subscribe(createdItem => {
        data.push(createdItem);
        this.odata = data;
        this.item = {};
      });
    }
    else {
      data[this.findSelectedItemIndex()] = this.item;
      this.crudService.update(this.options.endpoint + '(' + this.item[this.options["id"]] + ')', this.item).subscribe(updatedItem => {
        this.odata = data;
        this.item = {};
      });
    }
    this.displayDialog = false;
  }

  delete() {
    this.crudService.delete(this.options.endpoint + '(' + this.item[this.options["id"]] + ')').subscribe();
    let index = this.findSelectedItemIndex();
    this.odata = this.odata.filter((val, i) => i != index);
    this.item = null;
    this.displayDialog = false;
  }

  onRowSelect(event) {
    this.newItem = false;
    this.item = this.cloneItem(event.data);
    this.displayDialog = true;
  }

  cloneItem(item: object): object {

    let newItem = {};
    this.options.model.forEach(column => {
      if (item.hasOwnProperty(column.field)) {
        if (column.type == 'date') {
          newItem[column.field] = new Date(item[column.field])
        } else {
          newItem[column.field] = item[column.field]
        }
      }
    });
    return newItem;
  }

  findSelectedItemIndex(): number {
    return this.odata.indexOf(this.selectedItem);
  }

}

export class Options {

  constructor(
    public model: Array<Model>,
    public endpoint: string,
    public id: string,
  ) { }

}

export interface Model {
  field: string;
  type: string;
  title: string;
  //width: string;
}