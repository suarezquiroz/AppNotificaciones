import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import {Car} from './car';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class CarService {

  constructor(private http: Http) {}
  
      getCarsSmall() {
          return this.http.get('http://localhost:3000/data')
                      .toPromise()
                      .then(res => <Car[]> res.json())
                      .then(data => { return data; });
      }
  
      getCarsMedium() {
          return this.http.get('http://localhost:3000/data')
                      .toPromise()
                      .then(res => <Car[]> res.json())
                      .then(data => { return data; });
      }
  
      getCarsLarge() {
          return this.http.get('http://localhost:3000/data')
                      .toPromise()
                      .then(res => <Car[]> res.json())
                      .then(data => { return data; });
      }

}
