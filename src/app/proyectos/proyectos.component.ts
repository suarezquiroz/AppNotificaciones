import { Component, OnInit } from '@angular/core';
import { Options } from '../data-table/data-table.component';
import { Model } from '../data-table/data-table.component';

@Component({
  selector: 'app-proyectos',
  templateUrl: './proyectos.component.html',
  styleUrls: ['./proyectos.component.css']
})
export class ProyectosComponent implements OnInit {

  constructor() { }

  proyectoModel: Array<ItemModel> = [
    { field: "Id", title: "Id", type: "number" },

    { field: "Nombre", title: "Nombre", type: "string" },

    { field: "Descripcion", title: "Descripcion", type: "string" },

  ]

  proyectosOptions: Options = new Options(this.proyectoModel, "../Odata/Proyectos", "Id")

  ngOnInit() {
  }

}

class ItemModel implements Model {
  constructor(
    public field: string,
    public title: string,
    public type: string
  )
  { }
}

