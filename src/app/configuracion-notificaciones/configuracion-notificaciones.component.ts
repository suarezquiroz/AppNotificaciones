import { Component, OnInit } from '@angular/core';
import { Options } from '../data-table/data-table.component';
import { Model } from '../data-table/data-table.component';

@Component({
  selector: 'app-configuracion-notificaciones',
  templateUrl: './configuracion-notificaciones.component.html',
  styleUrls: ['./configuracion-notificaciones.component.css']
})
export class ConfiguracionNotificacionesComponent implements OnInit {

  constructor() { }

  configuracionNotificacionModel: Array<ItemModel> = [

    { field: "Id", title: "Id", type: "number" },

    { field: "IdPlantilla", title: "IdPlantilla", type: "number" },

    { field: "DominioCorreo", title: "Dominio Correo", type: "string" },
    
    { field: "CorreoRemitente", title: "Correo Remitente", type: "string" },
    
    { field: "ClaveRemitente", title: "Clave Remitente", type: "string" },

    { field: "FrecuenciaNotificacion", title: "FrecuenciaNotificacion", type: "number" },

  ]

  configuracionNotificacionOptions: Options = new Options(this.configuracionNotificacionModel, "../Odata/ConfiguracionNotificaciones", "Id")

  ngOnInit() {
  }

}

class ItemModel implements Model {
  constructor(
    public field: string,
    public title: string,
    public type: string
  ) { }
}
