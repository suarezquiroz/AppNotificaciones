import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfiguracionNotificacionesComponent } from './configuracion-notificaciones.component';

describe('ConfiguracionNotificacionesComponent', () => {
  let component: ConfiguracionNotificacionesComponent;
  let fixture: ComponentFixture<ConfiguracionNotificacionesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfiguracionNotificacionesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfiguracionNotificacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
