import { Component, OnInit } from '@angular/core';
import { Options } from '../data-table/data-table.component';
import { Model } from '../data-table/data-table.component';

@Component({
  selector: 'app-plantillas',
  templateUrl: './plantillas.component.html',
  styleUrls: ['./plantillas.component.css']
})
export class PlantillasComponent implements OnInit {

  constructor() { }

  plantillaModel: Array<ItemModel> = [

    { field: "Id", title: "Id", type: "number" },

    { field: "Asunto", title: "Asunto", type: "string" },

    { field: "HTML", title: "HTML", type: "string" },

  ]

  plantillasOptions: Options = new Options(this.plantillaModel, "../Odata/Plantillas", "Id")

  ngOnInit() {
  }

}

class ItemModel implements Model {
  constructor(
    public field: string,
    public title: string,
    public type: string
  )
  { }
}