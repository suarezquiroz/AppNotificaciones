import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class CrudService {

  constructor(private http: HttpClient) { }
  read(url) {
    return this.http.get(url)
  }

  //write needed methods
  create(uri, data) {
    return this.http.post(uri, data
      // ,
      // {
      //   headers: new HttpHeaders().set('Content-Type', 'application/json'),
      // }
    )
  }

  update(uri, data) {
    return this.http.patch(uri, data
      // , {
      //   headers: new HttpHeaders().set('Content-Type', 'multipart/form-data'),
      // }
    )
  }

  delete(uri) {
    return this.http.delete(uri
      // , {
      //   headers: new HttpHeaders().set('Content-Type', 'application/json'),
      // }
    )
  }


}
