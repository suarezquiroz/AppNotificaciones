import { Component, OnInit } from '@angular/core';
import { Options } from '../data-table/data-table.component';
import { Model } from '../data-table/data-table.component';

@Component({
  selector: 'app-notificaciones',
  templateUrl: './notificaciones.component.html',
  styleUrls: ['./notificaciones.component.css']
})
export class NotificacionesComponent implements OnInit {

  constructor() { }

  notificacionModel: Array<ItemModel> = [

    { field: "Id", title: "Id", type: "number" },

    { field: "IdProyecto", title: "IdProyecto", type: "number" },

    { field: "IdPlantilla", title: "IdPlantilla", type: "number" },

    { field: "IdConfiguracionNotificacion", title: "Configuracion Notificacion", type: "number" },

    { field: "Destinatarios", title: "Destinatarios", type: "string" },

    { field: "FechaCreacion", title: "Fecha Creacion", type: "date" },

    { field: "FechaTerminacion", title: "Fecha Terminacion", type: "date" },

    { field: "ConsultaREST", title: "ConsultaREST", type: "string" },
    
    { field: "TipoEnvio", title: "TipoEnvio", type: "string" },

    { field: "DatosJSON", title: "DatosJSON", type: "string" },

    { field: "Estado", title: "Estado", type: "string" }

  ]

  notificacionesOptions: Options = new Options(this.notificacionModel, "../Odata/Notificaciones", "Id")

  ngOnInit() {

  }

}

class ItemModel implements Model {
  constructor(
    public field: string,
    public title: string,
    public type: string
  ) { }
}

